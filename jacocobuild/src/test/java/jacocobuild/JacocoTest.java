package jacocobuild;

import static org.junit.Assert.*;

import org.junit.Test;

import accumurate.Sum;

public class JacocoTest {

	@Test
	public void sumTest1() {
		Sum sum = new Sum(1,2);
		assertEquals(3, sum.addThis());
	}
	
	@Test
	public void sumTest2() {
		Sum sum = new Sum(1,2);
		assertEquals(3, sum.add(2,1));
	}

}
